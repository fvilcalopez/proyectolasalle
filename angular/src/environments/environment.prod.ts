export const environment = {
    production: true,
    hmr       : false,
    apiHost: 'https://mantenimientocode.xyz/lumen/public/api',
    smartHost: '${SMARTHOST}',
    imageURL: '${IMAGEURL}',
    facebook_client_id: '${FACEBOOK_CLIENT_ID}',
    orcid_client_id: '${ORCID_CLIENT_ID}',
    google_client_id: '${GOOGLE_CLIENT_ID}'
};


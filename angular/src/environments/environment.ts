// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    hmr       : false,
    apiHost: 'http://localhost:8000/api',
    // apiHost: 'https://mantenimientocode.xyz/lumen/public/api',
    facebook_client_id: '762346860867602',
    orcid_client_id: 'APP-VPK7ZX7RIKJ89WS4',
    google_client_id: '587052392640-fv8hqhcd6nr6hrcqpu13m2vfn7b3idsa.apps.googleusercontent.com'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import {MatPaginatorIntl} from '@angular/material';

export class MyCustomPaginatorIntl extends MatPaginatorIntl {
  itemsPerPageLabel = 'Items por Página:';
  nextPageLabel = 'Siguente';
  previousPageLabel = 'Anterior';
  firstPageLabel = 'Primera Página';
  lastPageLabel = 'Última  Página';
  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length == 0 || pageSize == 0) { return `0 de ${length}`; }
    if (page == 0 || pageSize == 10) { return `1 - ${Math.min( pageSize, length )}  de ${length}`; }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;



    return `${startIndex + 1} - ${endIndex} de ${length}`;
  }
}

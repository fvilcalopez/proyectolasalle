import { AbstractControl, ValidatorFn} from "@angular/forms";

export function MatchPassword(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let password = undefined;
    let passwordConfirm = undefined;
    if (control.parent !== undefined) {
      password = control.parent.controls['password'].value;
    }
    if (control.parent !== undefined) {
      passwordConfirm = control.parent.controls['passwordConfirm'].value;
    }
    if(passwordConfirm == password && password != undefined && passwordConfirm != undefined)
    {
      if( control.parent.controls['password'].errors != null && control.parent.controls['password'].errors.hasOwnProperty('matchPassword')){
        delete control.parent.controls['password'].errors['matchPassword'];
        if ( Object.getOwnPropertyNames(control.parent.controls['password'].errors).length == 0 )
          control.parent.controls['password'].updateValueAndValidity();
      }
      if(control.parent.controls['password'].errors != null && control.parent.controls['passwordConfirm'].errors.hasOwnProperty('matchPassword')){
        delete control.parent.controls['passwordConfirm'].errors['matchPassword'];
        if ( Object.getOwnPropertyNames(control.parent.controls['passwordConfirm'].errors).length == 0 )
          control.parent.controls['passwordConfirm'].updateValueAndValidity();

      }

    }
    return passwordConfirm != password ?  { 'matchPassword': true } : null;
  };
}

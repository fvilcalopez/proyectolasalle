import { Injectable } from '@angular/core';
import { Prueba } from '../models/Prueba';
import { Observable } from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {PersonaService} from '../api/persona.service';

@Injectable({
  providedIn: 'root'
})
export class PruebaService {

  API_URI = 'http://localhost:8000/api/Persona';

  constructor(private http: HttpClient) { }

  getGames() {
    return this.http.get(`${this.API_URI}/Persona`);
  }

  getGame(id: string) {
    return this.http.get(`${this.API_URI}/Persona/${id}`);
  }

  deleteGame(id: string) {
    return this.http.delete(`${this.API_URI}/Persona/${id}`);
  }

  saveGame(prueba: Prueba) {
    return this.http.post(`${this.API_URI}/games`, prueba);
  }

  updateGame(id: string|number, updatedGame: Prueba): Observable<Prueba> {
    return this.http.put(`${this.API_URI}/games/${id}`, updatedGame);
  }


}

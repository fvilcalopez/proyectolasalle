export const navigation = [
  // {
  //   'id': 'indicadores',
  //   'title': 'Usuarios',
  //   'type': 'item',
  //   'icon': 'bar_chart',
  //   'url': '/app/administracion'
  // },
  {
    'id'   : 'person',
    'title': 'Personas',
    'type' : 'item',
    'icon' : 'group',
    'url': '/app/personas'
  },

  {
    'id'   : 'salid',
    'title': 'Visitante',
    'type' : 'item',
    'icon' : 'group',
    'url': '/app/entradas-salidas'
  }


];

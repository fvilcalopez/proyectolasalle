import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LinksService} from '../../../api/links.service';
import {MatDialog} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../api/user.service';
import {FuseConfigService} from '../../../../@fuse/services/config.service';
import {DomSanitizer} from '@angular/platform-browser';
import {AuthenticationService} from '../../login/services/authentication.service';
import {HttpClient} from '@angular/common/http';
import {AlertDialogComponent} from '../../../general/componentes/alert-dialog/alert-dialog.component';
import {Subscription} from 'rxjs';

@Component({
  selector: 'fuse-app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {
  public mailFormGroup: FormGroup;
  loader: boolean;
  dialogRef: any;
  constructor(private linkService: LinksService,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private fuseConfig: FuseConfigService,
              private sanitizer: DomSanitizer,
              private authenticationService: AuthenticationService,
              private httpClient: HttpClient) { }

  ngOnInit() {
    this.initUSuarioFormGroup();
    this.loader = false;

  }

  initUSuarioFormGroup(){
    this.mailFormGroup = new FormGroup({
      'email':            new FormControl('', [Validators.required, Validators.email]),
    });
  }

  dialogs(responseService: string, message: string, header: string, error: any[], errormessage: string){

    this.dialogRef = this.dialog.open(AlertDialogComponent, {
      data      : {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        errormessage: errormessage
      }});
  }

  registrar(){
    if(this.loader){
      return;
    }
    this.loader = true;
    let email = '';

   email = this.mailFormGroup.get('email').value;

   this.linkService.solicitarRecuperarPass(email).subscribe(
     data => {
       if (data){
         this.loader = false;

         this.dialogs('alert-succesful', 'Su solicitud de recuperación de contraseña fue enviada', '¡Éxito!', null, data.message);
         this.router.navigate( ['/reset-pass']);
       }
     },
     error => {
       // console.log(error.error.subErrors);
       this.loader = false;
       this.dialogs('alert-error', 'Ocurrió algún error mientras su solicitud se enviaba.', 'Upss..', error.error.subErrors, error.error.message);

     }
   );

  }

}

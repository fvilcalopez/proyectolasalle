import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {IMessage} from '../../../api/Model';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {LinksService} from '../../../api/links.service';
import {AlertDialogComponent} from '../../../general/componentes/alert-dialog/alert-dialog.component';

@Component({
  selector: 'fuse-app-verify-pass-emaill',
  templateUrl: './verify-pass-emaill.component.html',
  styleUrls: ['./verify-pass-emaill.component.scss']
})
export class VerifyPassEmaillComponent implements OnInit, OnDestroy {

  public sub: Subscription;
  public massageActivate: IMessage;
  public MessageType = '';
  public loader: boolean;
  dialogRef: any;
  constructor(private route: ActivatedRoute,
              public dialog: MatDialog,
              private router: Router,
              private linkService: LinksService) {
    this.loader = true;
  }

  dialogs(responseService: string, message: string, header: string, error: any[], errorsmessage: string){

    this.dialogRef = this.dialog.open(AlertDialogComponent, {
      data      : {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        errormessage: errorsmessage
      }});
  }

  ngOnInit() {
    this.sub =  this.route.queryParams.subscribe(
      params => {
        if (params.codigo){
          this.MessageType = 'Activando';
          this.linkService.validarSolicitudPass(params.codigo).subscribe(
            data => {
              this.loader = false;
              this.massageActivate = data;
              this.dialogs('alert-succesful', this.massageActivate.message , '¡Éxito!', null, '');
              this.router.navigate( ['/reset-pass/recovery-pass'], {queryParams: {codigo: params.codigo}});
            },
            error => {
               
              this.loader = false;
              this.dialogs('alert-error', 'Ocurrió algún error en la activación de su cuenta.', 'Upss..', error.error.subErrors, error.error.message);
              this.MessageType = 'Enviado';
            }
          );

        } else {
          this.MessageType = 'Enviado';
        }
      }
    );
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  reenviar(){
    this.router.navigate( ['/reset-pass/confirm-email']);
  }

}

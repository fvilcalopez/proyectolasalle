import {Injectable, Injector} from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse, HttpClient, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpUserEvent
} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {
  Router,
  Event,
  NavigationStart, RoutesRecognized, NavigationEnd, NavigationCancel, NavigationError
} from '@angular/router';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import {EmptyObservable} from 'rxjs/observable/EmptyObservable';
import {catchError, filter, take, switchMap, finalize} from 'rxjs/operators';
import {AuthenticationService} from '../services/authentication.service';

declare var swal: any;

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  isRefreshingToken: boolean = true;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor(private inj: Injector, private router: Router,
              private authService: AuthenticationService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(this.addToken(request))
      .pipe(
        catchError((error, ca) => {
          if (error instanceof HttpErrorResponse) {
            switch ((<HttpErrorResponse>error).status) {
              case 401:
                return this.handle401Error(request, next);
              default:
                return ErrorObservable.create(error);
            }
          } else {
            return ErrorObservable.create(error);
          }
        })
      );
  }

  addToken(req: HttpRequest<any>): HttpRequest<any> {
    const token = this.authService.getToken();
    if (token && token.token && !req.headers.has('Authorization')) {
      return req.clone({setHeaders: {Authorization: 'Bearer ' + token.token}});
    }
    return req;

  }

  handle400Error(error) {
    console.log('400 error');
    if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
      // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
      return this.logoutUser();
    }

    return EmptyObservable.create();
  }


  handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    if (this.isRefreshingToken) {
      this.isRefreshingToken = false;

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);
      //  const http = this.inj.get(HttpClient);


      return this.authService.refrescarSesion().pipe(
        switchMap((token) => {
          token = this.authService.getToken();
          if (token) {
            this.tokenSubject.next(token);
            return next.handle(this.addToken(req));
          }

          console.log('refresh failed');
          // If we don't get a new token, we are in trouble so logout.
          // this.logoutUser();
          return EmptyObservable.create();

        }), catchError((e: any) => {
          console.log('error  2' + e);
          // If there is an exception calling 'refreshToken', bad news so logout.
          // this.logoutUser();
          return EmptyObservable.create();
        }),
        finalize(() => {
          console.log('token finally)');
          this.isRefreshingToken = true;
        }));

      //  )
    } else {
      console.log('this.tokenSubject ' + JSON.stringify(this.tokenSubject));
      console.log('this.tokenSubjec2 ' + JSON.stringify(this.tokenSubject.filter(token => token != null)));
      return this.tokenSubject
        .filter(token => token != null)
        .take(1)
        .switchMap(token => {
          return next.handle(this.addToken(req));
        });
    }
  }

  logoutUser() {
    this.authService.logout(true);
    location.reload(true);
    return ErrorObservable.create('');
  }


}

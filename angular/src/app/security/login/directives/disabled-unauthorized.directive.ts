import { Directive, ElementRef, OnInit , Input } from '@angular/core';
import {Role} from "../models/roles";
import {AuthenticationService} from "../services/authentication.service";


@Directive({
  selector: '[DisabledUnauthorized]'
})
export class DisabledUnauthorizedDirective implements OnInit {

  private _permissions: Role[];

  @Input('DisabledUnauthorized') set permissions(permissions: Role[]){
    this._permissions = permissions;
    this.ngOnInit();
  }

  constructor(private el: ElementRef,
              private authorizationService: AuthenticationService) {
    this.authorizationService.observable.subscribe(login => this.ngOnInit())
    setTimeout(() => this.ngOnInit(), 50);
  }
  ngOnInit() {
    if (!this.authorizationService.isLogin()){
      this.el.nativeElement.disabled = true;
    } else if(this._permissions==null) {
      this.el.nativeElement.disabled = false;
    } else if(!this.authorizationService.hasRolPermision(this._permissions)){
      this.el.nativeElement.disabled = true;
    } else {
      this.el.nativeElement.disabled = false;
    }
  }
}

import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {IPageable, IPageRequestX, IPersona} from './Model';
import {Observable} from 'rxjs';
import {PaginationService} from './PaginationService';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  private url = environment.apiHost ;
  constructor(private httpClient: HttpClient) { }
  listarPersonas(nombre: string, tipo_persona: number, numero?: string, tipo_visitante?: string, page?: IPageRequestX): Observable<IPageable<IPersona>>{
    let params = new HttpParams();
    params = PaginationService.getPageParms(page);
    let nulable = '';
    (tipo_persona != null) ? params = params.set('tipo_persona',  tipo_persona + '') : nulable = '';
    (nombre != null) ? params = params.set('busquedacomplete',  nombre + '') : nulable = '';
    (numero != null) ? params = params.set('numero',  numero + '') : nulable = '';
    (tipo_visitante != null) ? params = params.set('tipo_visitante',  tipo_visitante + '') : nulable = '';
    return this.httpClient.get<IPageable<IPersona>>(this.url + '/Persona', { params: params }).pipe();
  }



  // detalleVigilancia(idVigilancia: number): Observable<IVigilancia> {
  //   return this.httpClient.get<IVigilancia>(this.url + '/vigilancias/' + idVigilancia).pipe();
  // }
  crearpersona(persona: IPersona): Observable<IPersona>{
    return this.httpClient.post<IPersona>(this.url + '/Persona', persona).pipe();

  }

  actualizarpersona(persona: IPersona, id): Observable<IPersona>{
    return this.httpClient.put<IPersona>(this.url + '/Persona/' + id, persona).pipe();
  }
//  actualizarVigilancia(vigilancia: IVigilancia, idVigilancia: number): Observable<IVigilancia>{
  //   return this.httpClient.put<IVigilancia>(this.url + '/vigilancias/' + idVigilancia, vigilancia).pipe();
  // }

  eliminarpersona(id: number): Observable<IPersona>{
    return this.httpClient.delete<IPersona>(this.url + '/Persona/' + id).pipe();
  }

  salidaPersona(id?: number): Observable<any>{
    // let params = new HttpParams();
    // params = params.set('salida',  new Date().getTime() + '');
    return this.httpClient.put<any>(this.url + '/Persona/salida/' + id, { salida: new Date().getTime() + '' }).pipe();
  }

  entradaPersona(id?: number,  entrada?: any): Observable<any>{
    // let params = new HttpParams();
    // params = params.set('salida',  new Date().getTime() + '');
    return this.httpClient.put<any>(this.url + '/Persona/entrada/' + id, entrada).pipe();
  }

}

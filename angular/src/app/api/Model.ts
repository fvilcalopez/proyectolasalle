import {Sort} from '@angular/material';

export interface IModel {
  id: number;
}

export interface IModelTimestamp extends IModel {
  deleted_at: Date;
  updated_at: Date;
  created_at: Date;
}

export class Creator {
  static page<I extends IModel>(): IPageable<I> {
    return {
      data: [],
      current_page: 0,
      from: 0,
      last_page: 0,
      per_page: 10,
      to: 0,
      total: 0
    };
  }
}

export interface IPageable<T extends IModel> {
  data: T[];
  current_page: number;
  from: number;
  last_page: number;
  per_page: number;
  to: number;
  total: number;
}

export interface IPageRequest {
  size: number;
  number: number;
  sort: Sort;
  all: boolean;

}

export interface IPageRequestX {
  sort: string;
  sortDirection: string;
  limit: number;
  page: number;
}

export interface IUser extends IModelTimestamp {
  password: string;
  name: string;
  lastname: string;
  email: string;
  role_id: number;
}

export interface IUserRegister extends  IUser{
  matchingPassword: string;
  facebook_id: string;
  orcid_id: string;
  google_id: string;
}

export interface IRole extends IModel {
  name: string;
}

export interface IRoleUser extends IModelTimestamp {
  user_id: number;
  role_id: number;
  role: IRole;
}

export interface IToken {
  token:      string;
  token_type: string;
  expires_in: number;
}

export interface IMessage {
  message: string;
}

export interface ICatalogo extends IModel{
  id: number;
  tipo: string;
  dato: string;
}

export interface ICarrera extends IModel{
  id: number;
  nombre: string;

}

export interface IPersonacarrera extends IModel{
  id: number;
  id_persona: number;
  id_carrera: number;
  tipo_percarrera: ICarrera;

}

export interface IPersona extends IModel{
  id: number;
  nombre: string;
  apellidos: string;
  numero: string;
  tipodoc_id: string;
  sunedu: string;
  // tipo_persona: Array<any>;
  tipopersona_id: any;
  tipo_doc: ICatalogo;
  tipo_persona: ICatalogo;
  tipo_idcarrera: any[];
}






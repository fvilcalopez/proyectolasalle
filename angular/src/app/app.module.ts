import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import 'hammerjs';
import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';

import {fuseConfig} from './fuse-config';

import {AppComponent} from './app.component';
import {FuseMainModule} from './main/main.module';

import {LoginComponent} from './security/login/login.component';
import {ErrorInterceptor} from './security/login/interceptors/error.interceptor';
import {JwtInterceptor} from './security/login/interceptors/jwt.interceptor';
import {FuseMainComponent} from './main/main.component';
import {LoginModule} from './security/login/login.module';
import {AuthGuard} from './security/login/guard/auth.guard';
import {ResetpassComponent} from './security/resetpass/resetpass.component';
import {ConfirmEmailComponent} from './security/resetpass/confirm-email/confirm-email.component';
import {VerifyPassEmaillComponent} from './security/resetpass/verify-pass-emaill/verify-pass-emaill.component';
import {RecoveryPassComponent} from './security/resetpass/recovery-pass/recovery-pass.component';
import {ResetpassModule} from './security/resetpass/resetpass.module';
import {MatInputModule} from '@angular/material';
import {RegisterComponent} from './security/register/register.component';
import {RegisterUserComponent} from './security/register/register-user/register-user.component';
import {RegisterModule} from './security/register/register.module';
import {VerifyEmailComponent} from './security/register/verify-email/verify-email.component';

const appRoutes: Routes = [
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'app',
      component: FuseMainComponent,
      canActivate: [AuthGuard]
    },
    { path: '', redirectTo: 'login', pathMatch: 'full' }

];

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes),
        TranslateModule.forRoot(),
        // Fuse Main and Shared modules
        FuseModule.forRoot(fuseConfig),
        FuseSharedModule,
        FuseMainModule,
        MatInputModule,
        // Modulos Propios
        LoginModule,
        ResetpassModule,
        RegisterModule
    ],
    bootstrap   : [
        AppComponent
    ],
    providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }]
})
export class AppModule
{
}

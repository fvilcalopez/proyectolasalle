import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';

import { locale as navigationEnglish } from './navigation/i18n/es';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector   : 'fuse-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.scss']
})
export class AppComponent
{

    constructor(
        private translate: TranslateService,
        private fuseNavigationService: FuseNavigationService,
        private fuseSplashScreen: FuseSplashScreenService,
        private fuseTranslationLoader: FuseTranslationLoaderService,
        private matIconRegistry: MatIconRegistry,
        private domSanitizer: DomSanitizer
    )
    {

        // Add languages
        this.translate.addLangs(['es']);

        // Set the default language
        this.translate.setDefaultLang('es');

        // Set the navigation translations
          this.fuseTranslationLoader.loadTranslations(navigationEnglish);

        // Use a language
        this.translate.use('es');

        this.matIconRegistry.addSvgIcon(
          'close-dialog',
          this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/close-dialog.svg')
        );

        this.matIconRegistry.addSvgIcon(
          'doc-file',
          this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/doc-file-icon.svg')
        );

        this.matIconRegistry.addSvgIcon(
          'txt-file',
          this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/txt-file-icon.svg')
        );

      this.matIconRegistry.addSvgIcon(
        'alert',
        this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/alert.svg')
      );

      this.matIconRegistry.addSvgIcon(
        'success',
        this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/check.svg')
      );

      this.matIconRegistry.addSvgIcon(
        'error',
        this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/error.svg')
      );

      this.matIconRegistry.addSvgIcon(
        'exit',
        this.domSanitizer.bypassSecurityTrustResourceUrl('assets/images/icons/exit.svg')
      );

    }
}

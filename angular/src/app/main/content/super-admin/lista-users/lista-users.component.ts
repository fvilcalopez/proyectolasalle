import {Component, OnInit, ViewChild} from '@angular/core';
import {Creator, IPageable, IUser} from '../../../../api/Model';
import {Subscription} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialog, MatSort, MatTableDataSource} from '@angular/material';
import {Router} from '@angular/router';
import {UserService} from '../../../../api/user/user.service';
import {AuthenticationService} from '../../../../security/login/services/authentication.service';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';
import {DetalleUsersComponent} from '../detalle-users/detalle-users.component';

@Component({
  selector: 'fuse-app-lista-users',
  templateUrl: './lista-users.component.html',
  styleUrls: ['./lista-users.component.scss']
})
export class ListaUsersComponent implements OnInit {

  public usuarios: IPageable<IUser>;
  public subData: Subscription;
  public listadata = null;
  public listarfromgroup: FormGroup;
  public displayedColumns: string[] = ['name', 'lastname', 'email', 'options'];
  dialogRef: any;
  dialogRefDetails: any;
  public id_user;

  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
              private router: Router,
              private Userservice: UserService,
              private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    // this.getUsuario();
    this.initListarFormGroup();
    this.initLista(1, 10);
  }

  initListarFormGroup() {
    this.listarfromgroup = new FormGroup({
      'name': new FormControl(''),
      'dni': new FormControl(''),
      'phone': new FormControl(''),
      'office': new FormControl(''),
      'filter': new FormControl('')
    });
  }

  initLista(page, size) {

    this.usuarios = Creator.page<IUser>();
    this.subData = this.Userservice.listarUsuarios('', {page: page, limit: size, sortDirection: null, sort: null}).subscribe(
      data => {

        this.usuarios = data;
        if (data.data.length > 0) {
          this.listadata = new MatTableDataSource(data.data);
          this.listadata.sort = this.sort;
        } else {
          this.listadata = null;
        }
        console.log(this.listadata);
      },
      error => {

        this.dialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
        this.initLista(1, 10);
      }
    );
  }

  reset() {
    this.listarfromgroup.reset();
    this.initLista(1, 10);
  }

  dialogs(responseService: string, message: string, header: string, error: any[], icon ?: string) {
    this.dialogRef = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  autorizacion() {
    this.dialogs('alert-error', 'No puedes eliminar tu propia cuenta. Contacta al soporte para cualquier duda.', 'Upss..', null, 'cancel');
  }

  delete(id: number) {
    this.dialogs('alert-confirmed', '¿Desea eliminar el cliente?', '¡Alerta!', null, 'alert');
    this.dialogRef.componentInstance.responseAcceptReject.subscribe((response: boolean) => {
      if (response === true) {

        this.Userservice.delete(id).subscribe(
          data => {
            console.log(data);

            if (data == null) {
              this.dialogs('alert-succesful', 'El usuario fue eliminado con  éxito', '¡Éxito!', null, 'success');
              this.initLista(1, 10);
            } else {
              this.dialogs('alert-error', 'El usuario tiene documentos ascociados, no se puede eliminar', 'Upss..', null, 'cancel');
            }
          },
          error => {

            this.dialogs('alert-error', error.error.message, 'Upss..', error.error.subErrors, 'error');
            this.initLista(1, 10);
          }
        );
      } else {

      }
    });
  }

  detail(id: number) {
    this.dialogRefDetails = this.dialog.open(DetalleUsersComponent, {
      data: {
        id: id
      }/*,disableClose: true*/
    });
    this.dialogRefDetails.afterClosed().subscribe(
      data => {
        if (data == 1) {
          this.initLista(1, 10);
        }
      });
  }

  paging(e) {
    this.initLista(e.number, e.size);
  }

  updateEvent() {
    this.initLista(1, 10);
  }

  getUsuario() {
    // const user = this.authenticationService.getUser();
    // console.log(user);
    // this.id_user = user.id;
    // id !=user.id? this.delete(id) : this.dialogs('alert-error', "No te puedes eliminar a ti mismo.", 'Upss..', null, 'error');
    // return !!user ? user.id : '';
  }

  applyFilter(filterValue: string) {
    this.listadata.filter = filterValue.trim().toLowerCase();

    if (this.listadata.paginator) {
      this.listadata.paginator.firstPage();
    }
  }

}

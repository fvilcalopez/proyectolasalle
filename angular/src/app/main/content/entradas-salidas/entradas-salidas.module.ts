import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '../../../../@fuse/shared.module';
import {
  MatAutocompleteModule,
  MatButtonModule, MatCardModule, MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatRippleModule, MatSelectModule,
  MatSidenavModule, MatSlideToggleModule, MatSortModule,
  MatStepperModule, MatTableModule, MatToolbarModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {PaginatorModule} from '../../../general/componentes/paginator/paginator.module';
import {LoginModule} from '../../../security/login/login.module';
import { ListarEntradasSalidasComponent } from './listar-entradas-salidas/listar-entradas-salidas.component';
import { CrearEntradasSalidasComponent } from './crear-entradas-salidas/crear-entradas-salidas.component';
import {DetallePersonaComponent} from '../persona/detalle-persona/detalle-persona.component';
import {CrearPersonaComponent} from '../persona/crear-persona/crear-persona.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {BrowserModule} from '@angular/platform-browser';
import {BarecodeScannerLivestreamModule, BarecodeScannerLivestreamOverlayModule} from 'ngx-barcode-scanner';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FuseSharedModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatInputModule,
    MatSidenavModule,
    MatMenuModule,
    MatRippleModule,
    MatStepperModule,
    MatToolbarModule,
    MatSelectModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCardModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    PaginatorModule,
    MatIconModule,
    MatDialogModule,
    MatDatepickerModule,
    MatMomentDateModule,
    LoginModule,
    BrowserModule,
    BarecodeScannerLivestreamModule,
    BarecodeScannerLivestreamOverlayModule
  ],

  declarations: [ListarEntradasSalidasComponent, CrearEntradasSalidasComponent, ],
  entryComponents: [ CrearEntradasSalidasComponent]
})
export class EntradasSalidasModule { }

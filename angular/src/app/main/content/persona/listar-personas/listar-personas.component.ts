import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {PersonaService} from '../../../../api/persona.service';
import {FormGroup} from '@angular/forms';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {CrearPersonaComponent, Tipoper} from '../crear-persona/crear-persona.component';
import {DetallePersonaComponent} from '../detalle-persona/detalle-persona.component';
import {AlertDialogComponent} from '../../../../general/componentes/alert-dialog/alert-dialog.component';

@Component({
  selector: 'fuse-app-listar-personas',
  templateUrl: './listar-personas.component.html',
  styleUrls: ['./listar-personas.component.scss']
})
export class ListarPersonasComponent implements OnInit {
  public listadata = null;
  public listadatacomplete: any;
  public displayedColumns: string[] = ['name', 'lastname', 'document', 'numero', 'carrera', 'persona', 'eliminar'];
  dialogRefAlert: any;
  tipoPersona: Tipoper[] = [
    {value: '4', viewValue: 'Visitante'},
    {value: '5', viewValue: 'Estudiante'},
    {value: '6', viewValue: 'Profesor'},
    {value: '8', viewValue: 'Administrativo'},
    {value: '9', viewValue: 'Limpieza'},
    {value: '10', viewValue: 'Seguridad'},
    {value: '11', viewValue: 'Egresado'},
  ];

  tipoPersonaSelecionado = '0';
  nombrePersona = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private personaService: PersonaService, public dialog: MatDialog) {
    this.listadata = new MatTableDataSource([]);
    this.paginator = this.listadata.paginator;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CrearPersonaComponent, {
      width: '450px'
    });
    dialogRef.afterClosed().subscribe( data => { this.ngOnInit(); });
  }

  openDialogDetalle(element): void {
    const dialogRef = this.dialog.open(DetallePersonaComponent, {
      width: '450px',
      data: element
    });
    dialogRef.afterClosed().subscribe( data => {  this.cargarData(1, 10); });
  }


  ngOnInit() {
    this.paginator.page.subscribe(
      (data: any) => {
        const index = data.pageIndex + 1;
        this.cargarData(index, data.pageSize);
      }
    );
    this.cargarData(1, 10);
  }

  cargarData(page, size){
    this.personaService.listarPersonas((this.nombrePersona == '') ? null : this.nombrePersona, Number(this.tipoPersonaSelecionado), null, null, {page: page, limit: size, sortDirection: null, sort: null}).subscribe(
      data => {
        if (data.data.length > 0) {
          this.listadata = new MatTableDataSource(data.data);
          this.listadatacomplete = data;
        } else {
          this.listadata = null;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  private onEnter(event: any)
  {
    if (event.keyCode == 13)
    {
      this.cargarData(0, 10);
    }
  }

  openAlertdialogs(responseService: string, message: string, header: string, error: any[], icon ?: string, icon_color ?: string) {
    this.dialogRefAlert = this.dialog.open(AlertDialogComponent, {
      data: {
        responseService: responseService,
        message: message,
        header: header,
        error: error,
        icon: icon
      }
    });
  }

  eliminarpersona( idpersona: number){

    this.openAlertdialogs('alert-confirmed', '¿Desea eliminar el componente?', '¡Alerta!', null, 'alert');
    this.dialogRefAlert.componentInstance.responseAcceptReject.subscribe((response: boolean) => {
      if (response === true) {
        this.personaService.eliminarpersona(idpersona).subscribe(
          data => {
            console.log(data);
            this.cargarData(1, 10);
          },
          error => {
            console.log(error);
          }
        );
      }
    });

  }
}



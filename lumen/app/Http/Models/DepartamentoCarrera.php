<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
class DepartamentoCarrera extends Model
{
    protected $table = "departamentocarrera";
    protected $primaryKey ="id";
    protected $fillable = ["id","id_departamento","id_carrera"];



}

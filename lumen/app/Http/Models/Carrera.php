<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
class Carrera extends Model
{
    protected $table = "carrera";
    protected $primaryKey ="id";
    protected $fillable = ["id","nombre","departamento_id"];


    public function departamento()
    {
        return $this->belongsTo('App\Http\Models\Departamento','departamento_id','id');
    }
}
